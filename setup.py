from setuptools import setup

setup(name='whatabout',
      version='0.1',
      description='A simple note manager',
      url='https://bitbucket.org/sgabert/whatabout',
      author='Stephan Gabert',
      author_email='stephan.gabert@fau.de',
      license='WTFPL',
      packages=['whatabout'],
      install_requires=[
          'tabulate',
      ],
      zip_safe=False)
