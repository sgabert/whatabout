#!/usr/bin/env python3

import argparse
import logging
import signal
import cmd
import os
import curses, curses.ascii, curses.textpad
from operator import itemgetter
from tabulate import tabulate
from datetime import datetime
from wa_db import WhatAboutDB
from wa_utils import read_editor, col_str, query_yn


class CmdProc(cmd.Cmd):
    def __init__(self, db, args):
        super(self.__class__, self).__init__()
        self.prompt = '(whatabout) '
        self.intro = 'Welcome to the WhatAbout CLI!'
        self.db = db

        if hasattr(args, 'add'):
            self.do_add('')
            exit()
        elif hasattr(args, 'del'):
            self.do_del(str(getattr(args, 'del')))
            exit()
        elif hasattr(args, 'edit'):
            self.do_edit(str(getattr(args, 'edit')))
            exit()
        elif hasattr(args, 'open'):
            self.do_open(str(getattr(args, 'open')))
            exit()
        elif hasattr(args, 'search'):
            arg = getattr(args, 'search')
            if arg is None:
                arg = ''
            self.do_search(arg)
            exit()

    def help_add(self):
        print('\tadd                      - add/create new note within editor (default: vim)')

    def help_del(self):
        print('\tdel     NID              - delete note with id NID')

    def help_edit(self):
        print('\tedit    NID              - edit note with id NID')

    def help_open(self):
        print('\topen    NID              - open/show note with id NID')

    def help_search(self):
        print('\tsearch                   - show all notes')
        print('\tsearch  PATTERN          - search notes: tags or title match the regexp PATTERN')

    def do_help(self, line):
        print('for specific help, type \'help <command>\'\n')
        self.help_add()
        self.help_del()
        self.help_edit()
        self.help_open()
        self.help_search()

    def do_EOF(self, line):
        return True

    def do_add(self, line):
        if len(line) != 0:
            self.help_add()
        else:
            add_note(self.db)

    def do_del(self, line):
        args = line.split()
        if len(args) != 1:
            self.help_del()
        else:
            if not query_yn("[CONFIRM] delete note '{0}'?".format(args[0])):
                return
            del_note(self.db, args[0])

    def do_edit(self, line):
        args = line.split()
        if len(args) != 1:
            self.help_edit()
        edit_note(self.db, args[0])

    def do_open(self, line):
        args = line.split()
        if len(args) != 1:
            self.help_edit()
        open_note(self.db, args[0])

    def do_search(self, line):
        args = line.split()
        if len(args) == 0:
            search_notes(self.db, '.*')
        elif len(args) == 1:
            search_notes(self.db, args[0])
        else:
            self.help_search()


def add_note(db):
    title, tags, content = read_editor()
    if title is None:
        return
    if tags is None:
        tags = ''
    if content is None:
        content = ''
    nid = db.add_note(title, content)
    if nid is None:
        logger.error('can\'t lookup note id of just created note')
        return
    tag(db, nid, tags.split())

def del_note(db, nid):
    # delete (/unlink) all tags of note
    untag(db, nid)
    db.del_note(nid)

def edit_note(db, nid):
    note = db.get_note_via_note_id(nid)
    if note is None:
        return
    old_title = note[2]
    old_tags = " ".join([t[1] for t in db.get_tags_via_note_id(nid)])
    old_content = db.get_note_content(note[0])
    new_title, new_tags, new_content = read_editor('{0}\n{1}\n\n{2}'.
                                                   format(old_title,
                                                          old_tags,
                                                          old_content))
    if new_title is None:
        # this indicates an error by the composer:
        # don't change the note
        return
    if new_tags is None:
        new_tags = ''
    if new_content is None:
        new_content = ''

    if new_tags != old_tags:
        to_untag_tags = [x for x in old_tags.split() if x not in new_tags.split()]
        # if a tag already exists add_tag only returns its id
        to_untag_tids = [db.add_tag(t) for t in to_untag_tags]
        untag(db, nid, to_untag_tids)
        tag(db, nid, new_tags.split())

    if old_title != new_title or old_content != new_content:
        db.update_note(nid, new_title, new_content)

def open_note(db, nid):
    note = db.get_note_via_note_id(nid)
    if note is None:
        return
    title = note[2]
    tags = " ".join([t[1] for t in db.get_tags_via_note_id(nid)])
    content = db.get_note_content(note[0])
    print('{0}\n{1}\n\n{2}'.format(title, tags, content))

def search_notes(db, pattern):
    # notes: set((note_id, last_mod, note_title))
    notes = set()
    if '.*' == pattern:
        for note in db.get_notes_all():
            notes.add(note)
    else:
        for note in db.get_notes_via_pattern(pattern):
            notes.add(note)
    notes = list(notes)
    if len(notes) == 0:
        return

    # add all tags to notes
    for idx in range(len(notes)):
        tags = " ".join(col_str(t[1], pattern) for t in db.get_tags_via_note_id(notes[idx][0]))
        # XXX shorter:: datetime.strptime[...].strftime("%Y-%m-%d"),
        notes[idx] = (notes[idx][0],
                      datetime.strptime(notes[idx][1], '%Y-%m-%d %H:%M:%S'),
                      col_str(notes[idx][2], pattern),
                      tags)

    notes.sort(key=itemgetter(0))
    print(tabulate(notes, headers=['nid', 'last mod', 'title' ,'tags']))

def tag(db, nid, tags):
    # check if note id exists
    if not db.get_note_via_note_id(nid):
        logger.error('note id \'{id}\': does not exist'.format(id=nid))
        return

    for t in tags:
        # create tag if needed, ignore if tag already exists
        tag_id = db.add_tag(t)
        # link the note to the tag
        if tag_id is not None:
            db.link(nid, tag_id)
        else:
            logger.error('can\'t lookup tag id of just created tag')

def untag(db, nid, tids=[]):
    if len(tids) == 0:
        tags = db.get_tags_via_note_id(nid)
        tids = [(tag[0]) for tag in tags]
    for tid in tids:
        db.unlink(nid, tid)

def _tui_parse_input(screen):
    '''parse every input character
       until [enter]: live regex search
       after [enter]: choose {[edit,show,del] note X, add note, exit}'''
    # change this to use different colors for highlighting (fg=black, bg=white)
    #curses.init_pair(1,curses.COLOR_BLACK, curses.COLOR_WHITE)
    #col_high = curses.color_pair(1)
    #col_norm = curses.A_NORMAL
    exit_tui = False

    # TODO global vars & change on terminal resize
    term_height, term_width = screen.getmaxyx()
    win_search_title = curses.newwin(1, 10, 0, 0)
    win_search_input = curses.newwin(1, term_width-10, 0, 11)
    win_results      = curses.newwin(term_height-2, term_width, 1, 0)
    win_status_line  = curses.newwin(1, term_width-2, term_height-1, 0)

    # any change in the window image: automatically refresh window
    win_status_line.immedok(True)
    win_search_input.immedok(True)
    win_search_title.immedok(True)
    win_results.immedok(True)

    while not exit_tui:
        win_search_input.clear()
        win_search_title.addstr(0, 0, '[search] ', curses.A_BOLD)
        win_status_line.addstr(0, 0, '[info] \'q\': exit, ...', curses.A_NORMAL)

        tb = curses.textpad.Textbox(win_search_input, insert_mode=True)
        tb.stripspaces = True
        search_pattern = tb.edit()
        # FIXME textpads are weird as fuck: no umlauts (setlocale does not work)
        #       and automatic ws-appending
        if len(search_pattern) > 1 and search_pattern[:-2] != '\\':
            search_pattern = search_pattern[:-1]
        win_results.clear()
        win_results.addstr(0, 0, "'{0}'".format(search_pattern), curses.A_NORMAL)
        x = win_results.getch()
        if x == ord('q'):
            exit_tui = True

def tui(db, args):
    # initialize a curses window
    screen = curses.initscr()
    # disable automatic input echoing
    curses.noecho()
    # disable line buffering
    curses.cbreak()
    # enable keypad
    screen.keypad(1)
    # enable colors for highlighting etc.
    curses.start_color()

    _tui_parse_input(screen)

def parse_args():
    '''parse the arguments initially'''
    parser = argparse.ArgumentParser(usage='%(prog)s [-h] [-tui] [--verbose]\n'
                                     '\t[\n'
                                     '\t --add              |\n'
                                     '\t --del NID          | --edit NID | --open NID |\n'
                                     '\t --search [PATTERN] |\n'
                                     '\t]',
                                     epilog='detailed help: see WhatAbout\'s CLI')
    parser.add_argument('--verbose',
                        '-v',
                        help='log debug infos',
                        action='store_true')

    parser.add_argument('-tui',
                        help='start in TUI mode (curses)',
                        action='store_true')

    mutex_group = parser.add_mutually_exclusive_group()
    mutex_group.add_argument('--add',
                             action='store_true',
                             default=argparse.SUPPRESS,
                             help=argparse.SUPPRESS)
    mutex_group.add_argument('--del',
                             type=int,
                             default=argparse.SUPPRESS,
                             help=argparse.SUPPRESS)
    mutex_group.add_argument('--edit',
                             type=int,
                             default=argparse.SUPPRESS,
                             help=argparse.SUPPRESS)
    mutex_group.add_argument('--open',
                             type=int,
                             default=argparse.SUPPRESS,
                             help=argparse.SUPPRESS)
    mutex_group.add_argument('--search',
                             type=str,
                             nargs='?',
                             default=argparse.SUPPRESS,
                             help=argparse.SUPPRESS)
    return parser.parse_args()

def main(args):
    # ignore ctrl-c
    signal.signal(signal.SIGINT, signal.SIG_IGN)

    # start the CLI with the wa_db as context
    with WhatAboutDB() as db:

        if args.tui:
            tui(db, args)
        else:
            CmdProc(db, args).cmdloop()


if __name__ == '__main__':
    # get the arguments
    args = parse_args()

    # configure the logger
    FORMAT = "[%(funcName)10s():%(lineno)s] %(message)s"
    LEVEL = logging.INFO
    if args.verbose is True:
        LEVEL = logging.DEBUG
    logging.basicConfig(format=FORMAT, level=LEVEL)
    logger = logging.getLogger(__name__)

    main(args)

    # cleanup
    curses.endwin()
    os.system('clear')
