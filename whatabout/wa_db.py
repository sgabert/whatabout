import sqlite3
import re
import logging
from os.path import expanduser

# tables ------------------------------------------
#  tags        (primary: tag_id)
#  notes       (primary: notes(id))
#  notes_tags  (foreign: notes(id), tags(id)))
# ------ ------------------------------------------

DB_PATH='~/.whatabout.db'
logger=logging.getLogger(__name__)


class WhatAboutDB:
    '''WhatAbout sqlite3-DB'''
    db = None
    dbc = None

    def __init__(self, path=None):
        if path is None:
            path = expanduser(DB_PATH)
        try:
            # XXX sqlite3.PARSE_DECLTYPES:
            #     fetch the actual fancy datatypes (like timestamp) from db
            self.db = sqlite3.connect(path, detect_types=sqlite3.PARSE_DECLTYPES)
        except sqlite3.OperationalError as ex:
            logger.error('can\'t open db path: \'{0}\'\n{1}'.format(path, ex))
            return None
        self.db.execute("PRAGMA foreign_keys = ON")
        self.dbc = self.db.cursor()

        # register the regex func (otherwise: OperationalError)
        self.db.create_function("REGEXP", 2, _regexp)

        # new db: create tables if they don't exist yet
        self.dbc.execute('''CREATE TABLE IF NOT EXISTS notes
                         (id INTEGER PRIMARY KEY AUTOINCREMENT,
                         title TEXT NOT NULL,
                         content TEXT,
                         last_mod TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL)''')

        self.dbc.execute('''CREATE TABLE IF NOT EXISTS tags
                         (id INTEGER PRIMARY KEY AUTOINCREMENT,
                         tag TEXT UNIQUE ON CONFLICT IGNORE,
                         counter INTEGER DEFAULT 0)''')

        self.dbc.execute('''CREATE TABLE IF NOT EXISTS notes_tags
                         (note_id INTEGER NOT NULL REFERENCES notes(id) ON DELETE CASCADE,
                         tag_id INTEGER NOT NULL REFERENCES tags(id) ON DELETE CASCADE,
                         PRIMARY KEY (note_id, tag_id) ON CONFLICT IGNORE)''')
        self.db.commit()

    def __enter__(self):
        return self

    # XXX To avoid exceptions (i.e. garbage collector removed obj before exit),
    #     always call with 'with X as X_obj:'
    #     __exit__ will then be called every time
    def __exit__(self, exc_type, exc_value, traceback):
        self.db.commit()
        self.db.close()

    def add_note(self, note_title, note_content):
        self.dbc.execute('''INSERT INTO notes(title, content) VALUES (?,?)''',
                         (note_title, note_content,))
        self.dbc.execute('''SELECT id FROM notes WHERE title = ?''',
                         (note_title,))
        ret = self.dbc.fetchall()
        if ret == []:
            return None
        return ret[0][0]

    def del_note(self, nid):
        self.dbc.execute('DELETE FROM notes WHERE id = ?',
                         (str(nid),))

    def update_note(self, note_id, title, content):
        self.dbc.execute('''UPDATE notes
                         SET
                         title = ?,
                         content = ?,
                         last_mod = CURRENT_TIMESTAMP
                         WHERE id = ?''',
                         (title, content, note_id))

    def add_tag(self, tag_name):
        # tag_name is unique (conflict: ignore) -> no need for existence-check
        self.dbc.execute('INSERT INTO tags(tag) VALUES (?)',
                         (tag_name,))
        self.dbc.execute('''SELECT id FROM tags WHERE tag = ?''',
                         (tag_name,))
        ret = self.dbc.fetchall()
        if ret == []:
            return None
        return ret[0][0]

    def get_notes_via_pattern(self, pattern):
        self.dbc.execute('''SELECT n.id, datetime(n.last_mod,'localtime'), n.title
                         FROM
                         notes AS n, tags AS t, notes_tags AS nt
                         WHERE
                         n.title REGEXP ?
                         OR
                         (
                         n.id = nt.note_id
                         AND
                         t.id = nt.tag_id
                         AND
                         t.tag REGEXP ?
                         )''',
                         (pattern, pattern))
        # fetchall(): returns a list of tuples
        return self.dbc.fetchall()

    def get_notes_all(self):
        self.dbc.execute('''SELECT id, datetime(last_mod,\'localtime\'), title
                         FROM notes''')
        return self.dbc.fetchall()

    def get_note_via_note_id(self, note_id):
        self.dbc.execute('''SELECT n.id, datetime(n.last_mod,'localtime'), n.title
                         FROM notes as n
                         WHERE n.id = ?''',
                         (str(note_id),))
        ret = self.dbc.fetchall()
        if ret == []:
            return None
        return ret[0]

    def get_note_content(self, note_id):
        self.dbc.execute('''SELECT content FROM notes WHERE id = ?''',
                         (str(note_id),))
        ret = self.dbc.fetchall()
        if ret == []:
            return None
        return ret[0][0]

    def get_tags_via_note_id(self, note_id):
        '''get list of tags of given note_id'''
        self.dbc.execute('''SELECT t.id, t.tag, t.counter
                         FROM
                         notes AS n, tags AS t, notes_tags AS nt
                         WHERE
                         nt.tag_id = t.id
                         AND
                         nt.note_id = n.id
                         AND
                         nt.note_id = ?''',
                         (str(note_id),))
        return self.dbc.fetchall()

    def link(self, note_id, tag_id):
        # return if note_id and tag_id are already linked
        if self.note_tag_islinked(note_id, tag_id):
            return
        self.dbc.execute('''INSERT INTO notes_tags(note_id, tag_id)
                         VALUES (?,?)''',
                         (note_id, tag_id,))
        self.dbc.execute('''UPDATE tags SET counter = counter + 1
                         WHERE id = ?''',
                         (tag_id,))

    def unlink(self, note_id, tag_id):
        if not self.note_tag_islinked(note_id, tag_id):
            return
        self.dbc.execute('''DELETE FROM notes_tags
                         WHERE note_id = ? AND tag_id = ?''',
                         (note_id, tag_id,))
        self.dbc.execute('''UPDATE tags SET counter = counter - 1
                         WHERE counter > 0 AND id = ?''',
                         (tag_id,))
        self.dbc.execute('''DELETE FROM tags
                         WHERE id = ? AND counter = 0''',
                         (tag_id,))

    def note_tag_islinked(self, note_id, tag_id):
        if self.dbc.execute('''SELECT EXISTS
                            (SELECT 1
                            FROM notes_tags
                            WHERE note_id = ? AND tag_id = ?
                            LIMIT 1)''',
                            (note_id, tag_id)).fetchone()[0]:
            return True
        return False


def _regexp(expr, item):
    reg = re.compile(expr)
    return reg.search(item) is not None
