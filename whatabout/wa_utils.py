import os
import tempfile
from subprocess import call
import re
import logging

logger = logging.getLogger(__name__)


def read_editor(init_msg='TITLE\nWS SEPARATED TAGS\n\nCONTENT\n(:cq for abort)'):
    editor_argv = [os.environ.get('EDITOR','vim'),
                   '+normal gg']
    title = None
    tags = None
    content = None
    with tempfile.NamedTemporaryFile(suffix=".tmp") as tf:
        editor_argv.append(tf.name)
        tf.write(bytes(init_msg, 'UTF-8'))
        tf.flush()
        if call(editor_argv) == 0:
            tf.seek(0)
            editor_input = tf.read().decode('utf-8')
            # strip leading whitespace characters
            raw = editor_input.lstrip()

            if len(raw.splitlines()) >= 1:
                title = raw.splitlines()[0]

            if len(raw.splitlines()) >= 2:
                tags = raw.splitlines()[1]

            if len(raw.splitlines()) >= 3:
                content = '\n'.join(raw.splitlines()[3:])
                if content.strip(' \n') == '':
                    logger.info("note _{0}_: empty".format(title))
    return title, tags, content

def query_yn(question, default='yes'):
    '''query yes/no question'''
    valid = {'yes': True, 'y': True, 'ye': True,
             'no': False, 'n': False}
    if default is None:
        prompt = ' [y/n] '
    elif default == 'yes':
        prompt = ' [Y/n] '
    elif default == 'no':
        prompt = ' [y/N] '
    else:
        logger.error('invalid default answer: \'{0}\')'.format(default))
        return False

    while True:
        print(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            print("Please respond with 'yes' or 'no' (or 'y' or 'n').\n")

def col_str(string, pattern):
    if '.*' == pattern:
        # do not higlight the catchall wildcard
        return string
    # convert string to raw string
    string = re.sub(pattern, _my_replace, string)
    return string

def _my_replace(match):
    match = match.group()
    # underline and colorize (red) the match
    return '\033[4m\033[95m' + match + '\033[0m'
