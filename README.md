# WhatAbout

A simple note manager with a handy CLI and a sqlite-based db.

Disclaimer:  
This is a work-in-progress project for fiddling around and wasting time,
don't use _whatabout_ as your single note taking tool.

## Usage

For a quick shell access it's recommended to add a bash function as a
wrapper for WhatAbout, as follows:

```
function wa() {
	WA="wa.py"
	case $* in
		"a"* ) shift 1; command $WA "--add"    "$@" ;;
		"d"* ) shift 1; command $WA "--del"    "$@" ;;
		"e"* ) shift 1; command $WA "--edit"   "$@" ;;
		"h"* ) shift 1; command $WA "--help"   "$@" ;;
		"o"* ) shift 1; command $WA "--open"   "$@" | less;;
		"s"* ) shift 1; command $WA "--search" "$@" ;;
		* ) command $WA "$@" ;;
	esac
}
```

### Help

Checkout WhatAbout's call arguments:
```
$ wa -h
usage: wa.py [-h] [--verbose]
	[
	 --add              |
	 --del NID          | --edit NID | --open NID |
	 --search [PATTERN] |
	]

optional arguments:
  -h, --help     show this help message and exit
  --verbose, -v  log debug infos

detailed help: see WhatAbout's CLI

$ wa
Welcome to the WhatAbout CLI!
(whatabout) ?
for specific help, type 'help <command>'

	add                      - add/create new note within editor (default: vim)
	del     NID              - delete note with id NID
	edit    NID              - edit note with id NID
	open    NID              - open/show note with id NID
	search                   - show all notes
	search  PATTERN          - search notes: tags or title match the regexp PATTERN
```

## TODO

* (ncurses-based?) TUI

## License

Copyright (C) 2017 Stephan Gabert (stephan `/dɑt/` gabert `/æt/` fau `/dɑt/` de)  

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the `COPYING` file for more details.
